%!TEX root = ../../main.tex

\section{Ideengenerierung}
\label{konzept:Ideation}

In der ersten Phase der \ac{ADM} kommt es darauf an, den Benutzer
mit wenig Ablenkung durch die Ideengenerierung zu führen. Der Erfolg dieser Phase
ist besonders wichtig für den weiteren Verlauf der Methode, da die späteren Phasen
auf den in dieser Phase generierten Ideen basieren und somit bei mangelhafter
Ideengenerierung auch kein gutes Endergebnis erzielt werden kann.

Die Ideengenerierung beginnt damit, dass die Benutzer sich die Fragestellung
noch einmal verdeutlichen und sicherstellen, dass keine Unklarheiten mehr bezüglich
der Fragestellung bestehen. Bei diesem Schritt handelt es sich um eine rein
kommunikative Aufgabe, daher muss in einer Umsetzung in \ac{AR} lediglich sichergestellt
werden, dass die Benutzer sich Zeit nehmen, die Fragestellung noch einmal zu
besprechen und Unklarheiten zu beseitigen.
Dies wird durch einen von den Benutzern zu bestätigenden Hinweis
umgesetzt, der an die Besprechung der Fragestellung erinnert und somit einen
festen Platz im Ablauf der Methodik für die Besprechung schafft.

Der zweite Schritt der Ideengenerierung besteht aus einem festen Zeitraum, in dem
die Benutzer einzeln ihre Ideen zu der Fragestellung aufschreiben. In einer Umsetzung
müssen hierzu zwei Funktionalitäten abgebildet werden. Zum einen muss der Benutzer
Ideen erstellen können und zum anderen muss das Zeitlimit eingehalten werden.

\subsection{Erstellung von Notizen}

Die Erstellung von Notizen ist ein wichtiger Bestandteil der Ideengenerierung
und muss in einer \ac{AR} Umsetzung ähnlich natürlich durchzuführen sein, wie klassisches
Schreiben auf Papier. Diese Funktionalität findet jedoch nicht nur bei der
Ideengenerierung Verwendung, sondern sollte auch in der gleichen Form für das
spätere Hinzufügen von neuen Ideen oder Überschriften verwendbar sein.
Daher ist es nicht nur wichtig, möglichst natürlich und mit wenig Aufwand einzelne
Notizen erstellen zu können, sondern auch verschiedene Farben für diese Notizen
auszuwählen, um unterschiedliche Ebenen von Gruppenüberschriften darzustellen.

\begin{samepage}
	\subsubsection{Eingabemethode}

	Zur Auswahl einer geeigneten Eingabemethode wurden zuerst mögliche Kandidaten
	identifiziert. Dabei zeigte sich, dass eine Umsetzung grundsätzlich mit den
	folgenden Eingabemethode möglich ist:
	\begin{itemize}[noitemsep]
		\item Virtuelle Tastatur
		\item Physische Tastatur
		\item Digitaler Stift
		\item Digitales Zeichentablett
		\item Fotografie
		\item Spracheingabe
	\end{itemize}
\end{samepage}

Die Eignung der möglichen Eingabemethoden wurde dann anhand verschiedener Kriterien
bewertet, zum einen in Bezug auf ihre jeweilige Eignung zur Texteingabe in
Verbindung mit der HoloLens, zum anderen in Bezug auf die Komplexität ihrer
Implementierung. Dabei wurden folgende Kriterien verwendet:
\begin{description}

	\item[Lesbarkeit]
	Bewertung der Lesbarkeit des Resultats für den Benutzer.

	\item[Komplexität der Interaktion]
	Beurteilung der Komplexität der Eingabemethode in ihrer Nutzung. Dabei sollte
	diese Komplexität möglichst gering sein, um möglichst wenig kognitiven Leistung
	des BEnutzers mit der Verwendung der Eingabemethode zu binden.

	\item[Unterstützung von Skizzierung]
	Bewertung der Unterstützung von Skizzierung auf Notizen.

	\item[Zusätzliche Hardwareanforderungen]
	Bewertung der Notwendigkeit zusätzlicher Hardware.
	Wobei die Notwendigkeit zusätzlicher Hardware negativ bewertet wird, um
	die Komplexität des Teils des Systems zu reduzieren, mit dem der Benutzer in
	Berührung kommt.

	\item[Komplexität der Implementierung]
	Grobe Einschätzung der Komplexität der Implementierung der jeweiligen
	Eingabemethode. Eine geringere Komplexität ist hier generell vorzuziehen.

\end{description}

\paragraph{Virtuelle Tastatur} ~\\

Die Eingabe über eine virtuelle Tastatur würde durch die Darstellung einer Tastatur
in AR vor dem Benutzer umgesetzt werden. Diese ähnelt stark der standardmäßig von
der HoloLens verwendeten Softwaretastatur. Der Benutzer wählt per Cursor und
\enquote{Air Tap}-Geste die einzugebenden Buchstaben aus.
Dies führt zu guter Lesbarkeit, da es sich um eine digitale Texteingabe handelt.
Jedoch werden aus diesem Grund keine Skizzen unterstützt.
Die Interaktion mit der virtuellen Tastatur ist jedoch bestenfalls mühsam und
für Benutzer, die noch wenig Erfahrung mit der HoloLens haben, stark gewöhnungsbedürftig,
da jeder einzelne Buchstabe durch Kopfbewegungen ausgewählt und per Geste bestätigt
werden muss. Zudem muss der Benutzer den Arm dauerhaft anheben, um die Hand
im Erfassungsbereich der HoloLens zu halten. Da es sich um eine virtuelle Eingabemethode
handelt, wird für die Umsetzung keine weitere Hardware benötigt. Zusätzlich ist auch
die Implementierung relativ simpel, da eine virtuelle Tastatur in Microsofts MixedRealityToolkit (MRTK)
\footnote{\url{https://github.com/Microsoft/MixedRealityToolkit-Unity}, Abgerufen 17.04.2018}
bereitgestellt wird.

\paragraph{Physische Tastatur} ~\\

Eine Eingabemethode mittels einer physischen Tastatur könnte durch eine einfache
Textvorschau in \ac{AR} und die Verwendung über eine an die HoloLens angeschlossene
Bluetooth Tastatur erfolgen. Dabei handelt es sich um eine digitale Texteingabe,
was zu einer guten Lesbarkeit, jedoch zu keiner Unterstützung für Skizzen führt.
Die Komplexität der Interaktion hängt von der Fähigkeit des Benutzers ab auf
Tastaturen zu schreiben, kann jedoch als relativ gering eingeschätzt werden.
An zusätzlicher Hardware wird eine Bluetooth Tastatur benötigt, die jedoch von
der HoloLens nativ unterstützt wird. Die Implementierung ist dabei trivial, da
Texteingabe in Unity vorgesehen ist und unverändert genutzt werden kann.

\paragraph{Digitaler Stift} ~\\

Ein digitaler Stift, wie er z.B. von Anoto
\footnote{\url{http://www.livescribe.com/en-us/smartpen/}, Abgerufen 17.04.2018}
angeboten wird, erlaubt die Digitalisierung von Handschrift auf speziellem Papier
und dem Benutzer könnte dann direkt seine Eingabe auf einer virtuellen Notiz in \ac{AR}
angezeigt werden. Diese Eingabemethode produziert eine digitale Handschrift, welche zwar
in der Lesbarkeit schlechter als digitaler Text einzuordnen ist, jedoch das Erstellen
von Skizzen unterstützt. Die Komplexität der Interaktion ist relativ gering, da
der digitale Stift wie ein herkömmlicher Stift verwendet werden kann und die Notiz
in \ac{AR} lediglich durch eine Geste bestätigt werden muss. Jedoch gibt es zum
Erstellungszeitpunkt dieser Arbeit noch keine native Unterstützung für die HoloLens,
sodass zusätzlich zu dem Stift weitere Hardware nötig ist. Da die fehlende direkte
Unterstützung und die deshalb notwendige Hardware viel Komplexität hinzufügen, ist
eine Implementierung dieser Methode innerhalb dieser Arbeit nicht möglich.

\paragraph{Digitales Zeichentablett} ~\\

Bei digitalen Zeichentabletts handelt es sich um spezielle Flächen, die Eingaben
über einen speziellen Stift erkennen können. Diese werden unter anderem von Wacom
\footnote{\url{https://wacom.com/en-de}, Abgerufen 17.04.2018}
angeboten und verhalten sich, was die betrachteten Kriterien betrifft, wie digitale
Stifte.

\paragraph{Fotografie} ~\\

Diese Eingabemethode kann durch Erstellung der Notizen auf bekannten Notizzetteln
und anschließender Digitalisierung durch Fotografieren mit der in der HoloLens
eingebauten Kamera umgesetzt werden. Dabei entsteht ein Foto der Handschrift des Benutzers, weshalb
die Lesbarkeit dieser Eingabemethode die schlechteste der betrachteten ist. Jedoch
wird durch diese Methode nicht nur die Skizzierung, sondern auch die Digitalisierung
von beliebig anderem Material unterstützt. Interaktion mit dieser Eingabemethode
ist wenig komplex, da die Erstellung der Notizen wie gewohnt ablaufen kann und nur
durch eine immer gleiche Geste zum Auslösen der Kamera ergänzt wird. Diese Eingabemethode
erfordert keine zusätzliche Hardware, da die HoloLens bereits mit einer Kamera ausgestattet
ist. Abhängig davon, wie stark der Aufnahmeprozess automatisiert und digital durch
Bildverbesserungsmaßnahmen unterstützt werden soll, ist der Aufwand der Implementierung
gering bis hoch.

\paragraph{Spracheingabe} ~\\

Die Erstellung von Notizen über Spracheingabe könnte mittels Sprachbefehlen und der
Erkennung natürlicher Sprache umgesetzt werden.
Dabei wird digitaler Text generiert, weshalb die Lesbarkeit gut ist und Skizzierung
nicht unterstützt wird. Die Komplexität der Interaktion ist abhängig von der
Fehlerrate der Spracherkennung. Fehler erfordern die Wiederholung der Eingabe,
verzögern so den Prozess der Ideengenerierung und steigern die Menge an Aufmerksamkeit,
die der Benutzer der Eingabemethode zukommen lassen muss. Spracheingabe erfordert
keine zusätzliche Hardware, da sie von der HoloLens bereits unterstützt wird.
Auch der Aufwand zur Implementierung ist relativ gering, da Sprachkommandos über
das MixedRealityToolkit bereits unterstützt werden und die Erkennung natürlicher
Sprache über bestehende Schnittstellen anderer Anbieter gelöst werden kann.

\paragraph{}
\label{Eingabe:Umsetzung}

\begin{table}[ht]\scriptsize
	\caption{Eingabemethoden im Vergleich}
	\label{table:Eingabemethoden}
	\begin{center}
		\begin{tabularx}{\textwidth}{X|X|X|X|X|X|X}

			& virtuelle Tastatur & physische Tastatur & digitaler Stift &  digitales Zeichentablett
			& Sprach- eingabe & Fotografie \\ \hline
			Lesbarkeit & + & + & - & - & + & - \\ \hline
			Interaktion & - & + & + & + & + & + \\ \hline
			Skizzierung & - & - & + & + & - & + \\ \hline
			Hardware- anforderungen & + & - & - - & - - & + & + \\ \hline
			Umsetzung & + & + & +++ & +++ & + & ++ \\ \hline
		\end{tabularx}
	\end{center}
\end{table}

Nach Auswertung der möglichen Methoden anhand der beschriebenen Kriterien
wurde sich für die Erstellung von Notizen über Fotografie entschieden.
Hierzu wechselt der Benutzer innerhalb der Anwendung in einen speziellen Modus.

Um den Benutzer während der Ideengenerierung nicht zu sehr mit der Digitalisierung
seiner Ideen abzulenken, wurde sich dazu entschieden dieses erst nach dem Ablauf des
Zeitlimits durchzuführen. Somit kann die eigentliche Ideengenerierung
im Vergleich zur klassischen \ac{ADM} fast unverändert durchgeführt
werden.

\subsection{Zeitlimit}

Nach der Temporal motivation theory \citep{doi:10.5465/amr.2006.22527462}
ist Zeit ein wichtiger motivierender Faktor. Dies lässt sich auch auf die
Ideengenerierung übertragen, weshalb es wichtig ist, einen festen Zeitrahmen für
diese auszuwählen. Zusätzlich sollte der Benutzer sich der verbleibenden Zeit bewusst
sein. Als Zeitrahmen empfehlen Gray et al. in ihrer Beschreibung der
\ac{ADM} 10 Minuten \citep{gray2010gamestorming}.
Der ideale Zeitrahmen ist jedoch auch von der Komplexität der zu bearbeitenden
Fragestellung abhängig. Da die \ac{ADM} jedoch grundsätzlich von
einer größeren Anzahl an Ideen profitiert, werden in der Umsetzung die 10 Minuten
unverändert übernommen.
Um das Interface während der Ideengenerierung möglichst wenig aufdringlich zu
gestalten, wird nach dem Start nur noch eine Uhr dargestellt, die die verbleibende
Zeit anzeigt. Diese folgt zur einfachen Erreichbarkeit dem Blick des Benutzers,
befindet sich jedoch im oberen Bereich des Blickfeldes, um beim Notieren von Ideen
nichts zu verdecken. Die letzte verbleibende Minute wird zusätzlich deutlich gemacht,
indem die Uhr ihre Farbe zu Rot wechselt. Zusätzlich sollen Hinweistöne, die
zu Beginn der letzten Minute und am Ende der Ideengenerierung ein akustisches Feedback
geben. Diese Hinweistöne ermöglichen es auch die HoloLens während der eigentlichen
Ideengenerierung abzulegen, sollte diese als störend empfunden werden.
Nach Ablauf der Zeit wird zusätzlich ein Dialog angezeigt, der den Benutzer über
den nächsten Schritt informiert. Nach Schließen des Dialogs wechselt die Anwendung
in den zuvor beschriebenen Aufnahmemodus. (siehe \autoref{Eingabe:Umsetzung})
