%!TEX root = ../../main.tex

\section{Benutzerschnittstelle}

Basierend auf dem bisherigen Konzept wird nun die Benutzerschnittstelle der
Anwendung für die einzelnen Phasen im Detail entworfen.

Für die Einrichtung werden zuerst die gemeinsamen Arbeitsbereiche benötigt. Diese
werden optisch an Whiteboards angelehnt und als weiße Fläche mit einem blauen
Rand dargestellt. Der Rand dient auch dazu, eine optisch gut erkennbare Eingrenzung
für den Arbeitsbereich zu bieten. Damit die Arbeitsbereiche als Landmarken dienen
können, müssen sie gut sichtbar und individuell gekennzeichnet werden.
Als simple Markierung dient dazu die Nummerierung der Arbeitsflächen,
die deutlich lesbar in der oberen linken Ecke vorgenommen wird.
Weiterhin müssen die Arbeitsflächen skaliert,
rotiert und verschoben werden können.
Dies wird über die gleichen Interaktionen abgebildet, die in der
Benutzerschnittstelle der HoloLens generell für diese Operationen verwendet werden.
So kann der Benutzer Arbeitsflächen verschieben, indem er den Cursor auf diese
bewegt und die \enquote{Drag}-Geste durchgeführt.
Zusätzlich wird der Arbeitsbereich von einer transparenten Box umschlossen, deren
Kanten blau eingefärbt sind. In der Mitte jeder Kante befindet eine blaue Kugel.
Mithilfe dieser Kugeln kann der Benutzer den Arbeitsbereich rotieren, indem er die
\enquote{Drag}-Geste ausführt, während er die Kugel mithilfe des Cursors selektiert hat.
Die Skalierung der Arbeitsfläche wird nach dem gleichen Prinzip durch an den Ecken
der zuvor beschriebenen Box platzierten Würfel ermöglicht.
Um den Benutzer zu den einzelnen Schritten der Einrichtung zu informieren, werden
Dialoge verwendet. Für diese werden die im MRTK enthaltenen Dialoge genutzt.
Die Bestätigung einzelner Schritte wird durch einen sich immer schräg unten vor dem Benutzer befindenden
Knopf ermöglicht.

Für die Ideengenerierung werden die gleichen Dialoge und Knöpfe zur Bestätigung
verwendet. Während der eigentlichen Ideengenerierung ist es zudem nötig, dem Benutzer
die verbleibende Zeit zu verdeutlichen und ihn nicht anderweitig abzulenken.
Deshalb wird in diesem Abschnitt der Cursor durch eine Anzeige der verbleibenden Zeit
ersetzt und alle anderen Anzeigen werden deaktiviert. Zusätzlich zu der Zeitanzeige ist es
sinnvoll, akustische Rückmeldungen zu verwenden.

\begin{figure}[h]
	\centering
	\begin{subfigure}{0.5\textwidth}
		\centering
		\includegraphics[width=0.8\linewidth]{assets/target-nonactive.png}
		\caption{Keine Hand erkannt}
		\label{konzept:ui:imagetargetnonactive}
	\end{subfigure}%
	\begin{subfigure}{0.5\textwidth}
		\centering
		\includegraphics[width=0.8\linewidth]{assets/target-active.png}
		\caption{Hand erkannt}
		\label{konzept:ui:imagetargetactive}
	\end{subfigure}
	\caption{Zustände des Cursors zur Digitalisierung}
	\label{konzept:ui:imagetarget}
\end{figure}

Anschließend wird der normale Cursor zur Digitalisierung durch eine Zielmarkierung
ausgetauscht. Diese Zielmarkierung erlaubt es dem Nutzer, zu bestimmen, wohin er
die zu fotografierende Notiz halten muss. Fotos werden dann über die
\enquote{Air Tap}-Geste der HoloLens ausgelöst und erzeugen eine neue virtuelle
Notiz, die den relevanten Bildausschnitt des Fotos enthält.
Diese wird dem Benutzer kurzzeitig neben der Zielmarkierung angezeigt, um eine
Überprüfung des Resultats zu ermöglichen.
Zusätzlich gibt der Cursor Feedback darüber, ob eine Hand des Benutzers sichtbar ist (siehe \autoref{konzept:ui:imagetarget}),
wenn ein Foto aufgenommen wird und wenn die Gefahr besteht, dass das Bild verwackelt wird,
weil der Benutzer beispielsweise seinen Kopf zu stark bewegt. Bei der Aufnahme des Fotos wird
zusätzlich eine akustische Rückmeldung ausgelöst. Des Weiteren behält der Cursor die
vorhandenen Interaktionsmöglichkeiten des normalen Cursors bei, weshalb eine Markierung im
Zentrum angezeigt wird, die das Zielen, wie der normale Cursor der Anwendung, ermöglicht.
Führt der Benutzer eine \enquote{Air Tap}-Geste aus, während die Markierung des Cursors
sich auf einem virtuellen Objekt befindet, wird die normale Interaktion ausgelöst
und kein Foto aufgenommen.
Beim Verlassen des speziellen Aufnahmemodus wird die Zielmarkierung wieder durch den
normalen Cursor der Anwendung ausgewechselt.

\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{assets/Untitled_Jam_Frame_6.png}
	\caption{Entwurf der Benutzerschnittstelle}
	\label{konzept:ui:sketch}
\end{figure}
Die Benutzerschnittstelle muss für die Präsentation und Strukturierung
vor allem die gemeinsamen Arbeitsbereiche und die von den Benutzern erstellten
Notizen darstellen.
Notizen werden als einfache flache Quader dargestellt, auf der das zugehörige Bild
dargestellt wird. Dies erzeugt ein für den Benutzer greifbares Objekt, dessen
vorrangiger Zweck die Informationsvermittlung ist.
Weiterhin wird vor dem Benutzer ein persönlicher Arbeitsbereich platziert. Dieser
ähnelt einem gemeinsamen Arbeitsbereich, ist jedoch an der Seite um ein Menü erweitert
und um 45\degree geneigt, um eine bessere Übersicht zu bieten. Damit der persönliche
Arbeitsbereich für den Benutzer immer verfügbar ist, folgt er den Bewegungen des Benutzers.
Um dabei nicht die restlichen Interaktionen des Benutzers zu behindern, wurde er
zusätzlich leicht unterhalb des normalen Sichtfeldes des Benutzers positioniert
und eine Möglichkeit vorgesehen, die Folgefunktion zu deaktivieren. \label{konzept:pinWs}
Noch nicht präsentierte Notizen des Benutzers werden auf dem persönlichen Arbeitsbereich
in einem Raster angezeigt.
Eine Skizze des Entwurfs der Benutzerschnittstelle ist in \autoref{konzept:ui:sketch}
zu sehen.
Als nächstes muss die Interaktion mit Notizen entworfen werden. Dabei ist die
wichtigste Interaktion das Bewegen der Notizen. Diese wird durch Auswahl mit dem
Cursor und Verwenden der \enquote{Drag}-Geste umgesetzt, was der allgemeinen,
vom Nutzerinterface der HoloLens verwendeten Methode, um Hologramme zu verschieben,
entspricht. Damit Benutzer sehen können, wo eine Notiz nach Ende der Interaktion
platziert wird, wird diese Stelle durch eine leicht transparente Version der Notiz
kenntlich gemacht. Bei der Bewegung von Notizen darf es allerdings nicht zu
Konflikten kommen, wenn zwei Benutzer die gleiche Notiz zur selben Zeit bewegen wollen.
Deshalb wird eine Notiz für weitere Interaktionen gesperrt, wenn ein anderer Benutzer
mit dieser interagiert. Diese Sperre wird zudem durch eine rote Färbung der Notiz angezeigt.
\begin{figure}[h]
	\centering
	\begin{subfigure}{0.5\textwidth}
		\centering
		\includegraphics[width=0.8\linewidth]{assets/20180607_025647_HoloLens_cropped.png}
		\caption{Präsentation}
		\label{konzept:präsentation}
	\end{subfigure}%
	\begin{subfigure}{0.5\textwidth}
		\centering
		\includegraphics[width=0.8\linewidth]{assets/20180607_025614_HoloLens_cropped.png}
		\caption{Hervorhebung}
		\label{konzept:hervorhebung}
	\end{subfigure}
	\caption{Präsentation und Hervorhebung von Notizen}
	\label{konzept:präsentation:hervorhebung}
\end{figure}
Durch einfache Anwendung der \enquote{Air Tap}-Geste auf eine Notiz kann ein Benutzer
diese hervorheben, dies ist als Ersatz für die klassische Geste des Zeigens gedacht.
Hervorgehobene Notizen werden grün markiert und mit einem Richtungsanzeiger gekennzeichnet (siehe \autoref{konzept:hervorhebung}).
Weiterhin wird die erste Bewegung einer Notiz nach Beginn der \enquote{Air Tap}-Geste leicht
verzögert, bis der Benutzer seine Hand um eine gewisse Distanz bewegt hat. Dies ermöglicht
die Umsetzung einer \enquote{Hold}-Geste, die nur ausgelöst wird, wenn der Benutzer
seine Hand nicht bewegt.
Diese \enquote{Hold}-Geste wird nun verwendet, um Notizen in einen Präsentationsmodus
zu versetzen.
Daher wird die in diesen Modus versetzte
Notiz auf ein Vielfaches ihrer normalen Größe skaliert, um so eine dominante Position
auf ihrem aktuellen Arbeitsbereich einzunehmen (siehe \autoref{konzept:präsentation}).
Auf einen Richtungsanzeiger, der Benutzer auf die Position der Notiz hinweist,
wird in diesem Modus verzichtet, da dieser die eigentliche Notiz verdecken könnte.
Zusätzlich wird eine Notiz in diesem Modus für andere Interaktionen gesperrt,
bis sie wieder in den normalen Zustand versetzt wurde. Dies ist durch erneutes
Anwenden der \enquote{Hold}-Geste auf die Notiz möglich.
Auf dem persönlichen Arbeitsbereich dargestellte Notizen verhalten sich
bei Bewegungsinteraktionen grundsätzlich wie zuvor beschrieben, lassen sich aber
nicht auf dem persönlichen Arbeitsbereich verschieben und werden beim Beenden
der Interaktion, solange sie noch auf den persönlichen Arbeitsbereich
ausgerichtet sind, wieder an ihren früheren Platz im Raster bewegt.
Des Weiteren können Notizen im persönlichen Arbeitsbereich nicht in den Präsentationsmodus
versetzt oder hervorgehoben werden, da diese Interaktionen nur sinnvoll sind,
wenn andere Benutzer die Notiz auch sehen können.
Die Erstellung weiterer Notizen erfordert den Wechsel der Anwendung in den
Aufnahmemodus. Um dies zu ermöglichen, befindet sich am persönlichen Arbeitsbereich
auch ein Menü, welches einen Knopf zum Starten und Beenden des Aufnahmemodus enthält.
Beim Wechsel in den Aufnahmemodus werden zusätzlich alle geteilten Arbeitsbereiche
ausgeblendet, um den Benutzer beim Digitalisieren der neuen Idee nicht zu behindern.
Auf ähnliche Art und Weise wird die Interaktion zur Archivierung von Notizen umgesetzt.
Hierbei wird durch einen Knopf im Menü in einen Modus gewechselt, in dem
das Durchführen der \enquote{Air Tap}-Geste auf eine Notiz diese archiviert. Um die
destruktive Natur dieser Operation deutlicher kenntlich zu machen, werden sowohl der
Knopf als auch der Cursor im Archivierungsmodus rot eingefärbt.
