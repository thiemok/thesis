%!TEX root = ../../main.tex

\chapter{Konzeption}

Zur Umsetzung einer Anwendung, die die Umsetzbarkeit der \ac{ADM} in
\ac{AR} untersucht, ist zunächst die Entwicklung eines Konzeptes nötig.
Dazu werden zunächst die durch die Methode und das Verhalten der Anwender bei der Durchführung
der Methode entstehenden Anforderungen identifiziert.
Danach wird für jede Phase der \ac{ADM} entsprechend der Anforderungen ein
Lösungsansatz ausgearbeitet. Und anschließend die Benutzerschnittstelle im Detail
entworfen.

\section{Anforderungen}

Da die \ac{ADM} in der Praxis in verschiedenen Varianten angewendet
wird (siehe \autoref{AffnityDiagramming}) und es im Rahmen dieser Arbeit nicht
möglich ist einen konfigurierbaren Ablauf zu unterstützen, muss zunächst eine
der Varianten für die Umsetzung ausgewählt werden. Varianten mit einer geringeren
Anzahl an Notizen sind dafür vorzuziehen, da sie eine geringere Dauer besitzen
und deshalb die Durchführung des abschließenden Usability-Tests erleichtert wird und
potenzielle Probleme durch das Gewicht der HoloLens reduziert werden. Zusätzlich werden
durch die kleinere Anzahl an Notizen geringere Anforderungen an die Hardware gestellt,
wodurch der Aufwand der Implementierung des Prototyps sinkt.
Zusätzlich ermöglicht die Integration der Ideengenerierung eine vollständigere
Abbildung der Methodik in einer Anwendung, da keine vorbereitenden Arbeitsschritte nötig sind.
Aus den genannten Gründen wurde das weitere Konzept auf diese Variante der
\ac{ADM} beschränkt.

Die praktische Durchführung der Methodik lässt sich in den folgenden Schritten
zusammenfassen:
\begin{description}

	\item [Vorbereitung des Raumes]
	Vor der Durchführung muss der Raum, in dem die \ac{ADM} durchgeführt
	werden soll, entsprechenden vorbereitet werden. Dies beinhaltet die Bereitstellung
	von Arbeitsmaterial und Arbeitsflächen, die sich zur Befestigung von Notizen eignen.

	\item [Ideengenerierung]
	Zur Generierung von Ideen wird zunächst die zu behandelnde Fragestellung besprochen
	und danach ein zeitlich beschränktes Brainstorming durchgeführt. Bei diesem schreibt
	jeder Teilnehmer für sich innerhalb des Zeitlimits möglichst viele Ideen zur
	vorgestellten Fragestellung einzeln auf Notizzettel.

	\item [Präsentation]
	Die zuvor generierten Ideen werden reihum einzeln vorgestellt und soweit erklärt,
	bis jeder Teilnehmer die vorgestellte Idee verstanden hat. Danach wird die Idee
	an einer zufälligen Stelle an der Wand befestigt.

	\item [Gruppierung]
	Die Ideen werden nun kollaborativ in Gruppen angeordnet und können bei Bedarf
	erweitert werden. Sind alle Ideen einer Gruppe zugeordnet, werden Überschriften
	für die Gruppen gebildet und auf andersfarbigen Notizzetteln notiert und über
	der jeweiligen Gruppe befestigt.
	Dieser Schritt kann dann mit den zuvor gebildeten Überschriften wiederholt werden,
	bis das gewünschte Abstraktionsniveau erreicht ist.

\end{description}

Eine virtuelle Umsetzung der \ac{ADM} muss jeden dieser Arbeitsschritte
vollständig unterstützen, eine vollständig virtuelle Abbildung ist jedoch nicht
unbedingt nötig, solange die Durchführung dadurch nicht eingeschränkt wird.

Zusätzlich muss auch weiteres, in der Methodik nicht spezifiziertes Verhalten
der Benutzer unterstützt werden, um einen möglichst nutzerfreundlichen und dem
Nutzer vertrauten Ablauf der Methode zu gewährleisten.
Eine Beobachtungsstudie \citep{geyer2011designing,Geyer20138619949} zur \ac{ADM}
untersuchte dazu das Verhalten von Anwendern der Methode.

In der Studie zeigte sich, dass Anwender während der Ideengenerierung auch immer
wieder bereits erstellte Ideen in die Hand nehmen, was der Reflexion und der Suche
nach Inspiration für weitere neue Ideen dient.

Die Präsentation der Ideen zeigte hauptsächlich einen häufigen
Aufmerksamkeitswechsel zwischen dem eigenen Arbeitsbereich, anderen Anwendern
und der gemeinsamen Arbeitsfläche. Dieser Aufmerksamkeitswechsel entstand dadurch,
dass Anwender während der Präsentation immer wieder ihre eigenen Ideen reflektierten.
Zusätzlich zeigte sich, dass Anwender sich vor der gemeinsamen Arbeitsfläche gegenseitig
blockierten.

Während der Gruppierung wechselten die Anwender zu intensiver kollaborativer Arbeit.
Dabei wurden individuelle Handlungen zur Kommunikation, Reflexion und Interaktion
beobachtet.
Während der Diskussion ist besonders Gruppenbewusstsein wichtig um einen gleichmäßigen
Kommunikationsfluss zu gewährleisten. Zusätzlich wurde beobachtet, dass gemeinsame
Referenzen durch gute Sichtbarkeit und Erreichbarkeit der gemeinsamen Arbeitsfläche
gefördert wurden.
Die Wichtigkeit von sprachlichen Referenzen in kollaborativen AR
Szenarien wurden auch von Chastine et al. in einer weiteren Studie \citep{Chastine:2007:UDS:1268517.1268552}
beobachtet.

Anhand dieser Beobachtungen lassen sich weitere Anforderungen an eine virtuelle
Umsetzung der Methode identifizieren.
Zum einen muss die Reflexion der bereits durch den Benutzer erstellten Ideen während
der Ideengenerierung und Präsentation unterstützt werden, da aus den Beobachtungen
hervorgeht, dass dies genutzt wird, um an Inspiration für weitere Ideen zu gelangen.
Dabei sollten die erstellten Ideen einfach und übersichtlich zugreifbar sein,
um unnötiges Suchen in den bereits erstellten Ideen zu vermeiden und somit die
Aufmerksamkeit für die Methodik mit unwichtigen Handlungen zu binden.
Zusätzlich sollten die Arbeitsflächen zu jedem Zeitpunkt für den Benutzer sichtbar
und nicht durch andere Objekte wie Interface-Elemente oder andere Benutzer verdeckt werden.
Und den Benutzern sollten ausreichende Möglichkeiten zur Referenzbildung
bereitstehen. Beides ist wichtig um eine effektive Kommunikation zwischen den
Benutzern zu ermöglichen.

Es müssen also in der Anwendung die Einrichtung des genutzten Raums, die Ideengenerierung
und die Präsentation und Strukturierung von Ideen umgesetzt und dabei
die zuvor identifizierten Anforderungen erfüllt werden.
Im Folgenden werden die einzelnen Abschnitte der Anwendung im Detail ausgearbeitet.

\input{src/2_konzeption/setup.tex}
\input{src/2_konzeption/ideation.tex}
\input{src/2_konzeption/strukturierung.tex}
\input{src/2_konzeption/ui.tex}
