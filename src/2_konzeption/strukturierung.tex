%!TEX root = ../../main.tex

\section{Präsentation und Strukturierung}
\label{Präsentation und Strukturierung}

Die zweite und dritte Phase der \ac{ADM} werden vorwiegend durch
das Präsentieren und Strukturieren der zuvor in Phase eins erstellten Notizen bestimmt.
Die Benutzer beschäftigen sich während dieser Phasen also besonders damit, Notizen
auf und zwischen den verschiedenen Arbeitsbereichen zu verschieben, diese und ihre
Anordnung zu diskutieren und Notizen einzeln den anderen Teilnehmern zu präsentieren.
Da diese beiden Phasen aus Benutzersicht in der gleichen Umgebung ablaufen und
die gleichen Interaktionsmittel benötigen, werden sie im weiteren Verlauf als eine
Phase betrachtet.

Aus den zur Durchführung der \ac{ADM} nötigen Handlungen und dem beobachteten
Verhalten von Anwendern der \ac{ADM} lassen sich nun folgende Interaktionen formulieren,
die in einer \ac{AR}-Umsetzung unterstützt werden müssen:
\begin{enumerate}
	\item[1.] Benutzer können Notizen in die geteilten Arbeitsflächen bewegen
	\item[2.] Notizen können von einer geteilten Arbeitsfläche auf eine andere bewegt werden
	\item[3.] Notizen können auf der Arbeitsfläche, auf der sie sich befinden, bewegt werden
	\item[4.] Benutzer können Notizen von einer Arbeitsfläche nehmen und diese betrachten
	\item[5.] Benutzer können Notizen anderen Teilnehmern präsentieren
	\item[6.] Benutzer können zeigen, auf welche Notiz sie schauen, ohne diese von ihrer
	Arbeitsfläche zu nehmen
	\item[7.] Notizen können nur von einem Benutzer und nicht von mehreren gleichzeitig bewegt werden
	\item[8.] Benutzer können sehen, wenn eine Notiz von einem anderen Benutzer bewegt wird
	\item[9.] Benutzer können auf ihre bereits erstellten, aber noch nicht präsentierten,
	Notizen zur Reflexion zurückgreifen
	\item[10.] Benutzer können eine Notiz löschen
	\item[11.] Benutzer können eine Notiz wieder an der Arbeitsfläche befestigen, ohne zusätzlichen
	Aufwand in ihre Positionierung investieren zu müssen
	\item[12.] Benutzer können sehen, wo eine Notiz auf der Arbeitsfläche platziert wird,
	während sie sie bewegen
	\item[13.] Benutzer können zusätzliche Notizen hinzufügen
	\item[14.] Benutzer können Gruppen mit Überschriften versehen
\end{enumerate}

Zunächst werden geteilte Arbeitsbereiche für die gemeinsame Arbeit mit den Notizen
benötigt. Diese werden wie in der Einrichtung platziert verwendet (siehe \autoref{setup}).
Zusätzlich erfordert die Umsetzung der Interaktionen 1 und 9 eine einfach erreichbare
Möglichkeit, um bisher noch nicht präsentierte Notizen betrachten und in die gemeinsamen
Arbeitsbereiche bewegen zu können. Um dies zu ermöglichen, wird vor dem Benutzer
ein persönlicher Arbeitsbereich platziert.

Die Interaktionen 1--8 und 11--12 betreffen die direkte Manipulation von Notizen.
Um diese Interaktionen umzusetzen, werden verschiedene Maßnahmen konzipiert.
Zuerst werden Notizen in alle Richtungen verschiebbar gemacht.
Diese Maßnahme betrifft Interaktionen 1--4.
Zusätzlich werden Notizen automatisch parallel zur nächsten Arbeitsfläche ausgerichtet,
sodass der Benutzer sich nicht mit der Ausrichtung von Notizen beschäftigen muss.
Dieses Verhalten ist an das allgemeine Verhalten von Anwendungsfenstern angelehnt,
jedoch werden Notizen gezielt nur an Arbeitsflächen ausgerichtet und nicht an erkannten
Wänden (Interaktionen 1--3 und 11).
Die Platzierung von Notizen wird des Weiteren dadurch erleichtert, dass Notizen
nach Beendigung der Interaktion automatisch im nächsten Punkt auf einer Arbeitsfläche
platziert werden. Dies verhindert auch,
dass Notizen verloren gehen können, da sie nicht frei im Raum platziert werden
können. Zusätzlich wird der Punkt, an dem die Notiz aktuell platziert werden würde,
hervorgehoben, sodass immer klar ist, wo die Notiz aktuell platziert wird,
wenn sie die Kontrolle des Benutzers verlässt (Interaktionen 1, 2, 11 und 12).

Wird eine Notiz von einem Benutzer bewegt, wird sie für die anderen Benutzer weiterhin
an ihrem ursprünglichen Platz angezeigt, bis die Interaktion beendet wurde.
Erst dann wird sie an ihren neuen Platz bewegt. Dies soll für eine ruhigere Interaktion
sorgen und unnötige und verwirrende Bewegungen, die von anderen Benutzern ausgelöst
wurden, im beschränkten Raum zwischen den Arbeitsbereichen vermeiden.
Zusätzlich werden Notizen, die gerade von anderen Benutzern manipuliert werden,
für zusätzliche Interaktionen gesperrt, um Konflikte zu vermeiden (Interaktionen 2--4, 7 und 8).

Weiterhin können Notizen in einen Präsentationsmodus versetzt werden.
Dieser Modus dient dazu, eine Notiz exklusiv zu besprechen. Die Notiz
soll daher für alle Benutzer deutlich erkennbar sein und kann aufgrund der exklusiven
Betrachtung auch andere Notizen verdecken (Interaktion 5).

Um dem Benutzer die Möglichkeit zu geben auf eine Notiz zu zeigen, können die
Notizen einfach hervorgehoben werden.
Die Notiz wird dann an ihrer Position farblich markiert und ein
Richtungsanzeiger erscheint, der auf die hervorgehobene Notiz hinweist (Interaktion 6).

Zur Erstellung weiterer Notizen kann die gleiche Interaktion wie während der
Ideengenerierung weiterverwendet werden (Interaktion 13). Diese Interaktion kann
gleichzeitig auch zur Erstellung von Überschriften verwendet werden, indem der
Benutzer andersfarbige Notizen benutzt, um die Überschrift aufzuschreiben (Interaktion 14).

Zuletzt wird eine Funktion zur Archivierung von Notizen benötigt, dies kann
durch Aktivierung eines speziellen Modus und Auswahl der zu archivierenden Notiz
geschehen (Interaktion 10. Archivierte Notizen können dann ähnlich noch nicht präsentierter Notizen
wiederhergestellt werden, indem sie auf dem persönlichen Arbeitsbereich angezeigt
werden.

\subsection{Referenzierung}

Da kollaboratives Arbeiten stark von effektiver Kommunikation abhängig ist, sollte
diese, soweit es im Rahmen der Anwendung möglich ist, unterstützt werden.
Besonders wichtig ist dabei die Unterstützung der Referenzierung von virtuellen
Objekten. So wurde von Chastine et al. eine Studie zum Thema der Referenzierung
in AR Umgebungen durchgeführt.
\begin{quote}
	Both guide and builder made heavy use of deictic speech
	(e.g. “this”, “that” and “here”), indicating that spoken references
	are important.
	\attrib{\citealp{Chastine:2007:UDS:1268517.1268552}}
\end{quote}
Dabei wurde beobachtet, dass Teilnehmer starken Gebrauch von deiktischer Sprache
machten, welches auf die Wichtigkeit gesprochener Referenzen hinweist.
Zusätzlich zeigte sich, dass Referenzen relativ zu bereits etablierten Positionen
gemacht wurden.
Die zuvor genannten Beobachtungen legen eine Verwendung von virtuelle Landmarken
nahe, da diese einen Ankerpunkt für relative Referenzierung bilden können
\citep{10.1007/3-540-48384-5_3}.
In einer weiteren Studie von Müller, Rädle und Reiterer wurde zusätzlich die Eignung
virtueller Landmarken zur Bildung eines gemeinsamen Referenzsystems beobachtet.
Außerdem zeigte sich, dass das Vorhandensein von Landmarken dabei half, Konflikte
bei der Referenzierung schneller zu lösen \citep{Muller:2017:RCM:3025453.3025717}.
Da Notizen nur auf gemeinsamen Arbeitsflächen platziert werden können und sie somit
nur in zwei Dimensionen relativ zur Arbeitsfläche referenziert werden müssen,
eignen sich die Arbeitsflächen besonders gut als virtuelle Landmarken.
Ihre begrenzte Anzahl, feste Position und Größe unterstützen zusätzlich die
leichte und eindeutige Referenzierbarkeit. Diese Argumente sprechen auch gegen
eine direkte Verwendung der Notizen als Landmarken.
Weiterhin liefert die Form der Arbeitsflächen gleichzeitig auch einen deutlich begrenzten
Rahmen für die Bildung von Referenzen.
Zudem besteht keine Gefahr Objekte durch die Landmarken zu verdecken,
da diese sich immer am äußeren Rand des virtuellen Raumes befinden.
Da virtuelle Landmarken für jeden Benutzer eindeutig identifizierbar sein müssen,
werden die Arbeitsflächen jeweils mit einer, für alle Benutzer gleichen, einzigartigen
Markierung versehen.
